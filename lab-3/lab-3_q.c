#include "lab-3_q.h"
#include <math.h>
#include <stdio.h>

// prototypes
int sign ( uint8_t x );
int8_t neg( int8_t x);
uint8_t mod( uint8_t x, int n );
int int2seq( bool* X, int8_t x );
int8_t seq2init( bool* X, int n);
void add_seq( bool* R, bool* X, bool* Y, int n );
void printBoolArray(bool *X, int n);

void rep( int8_t x )
{
  printf( "%4d_{(10)} = ", x );

  for( int i = ( BITSOF( x ) - 1 ); i >= 0; i-- )
  {
    printf( "%d", ( x >> i ) & 1 ); // x>>i shift the binary representation of x right by i digits. So, on first iteration, shifts left most bit to first bit. On second iteration shifts second left most bit to first bit, and left most bit to second bit, and so on. The other bits are replaced with 0s (So, if x is 8 bits, and we let binary representation of x= x_7x_6...x_1, and i=6, x>>i will leave: 000000x_7x_6). Then, ANDs this with 0..01 (binary representation of 1{(10)} ), which returns a bit string with the result of doing ith bit of x AND ith bit of 0..01. This will then convert all the bits apart from the right most one to 0, and leave the right most bit as it was. This is then converted to base 10, ie the number 1 or 0, and then printed to stdout. The end result of this loop is that the binary representation of x is printed out.
  }

  printf( "_{(2)}\n" );
}

int main( int argc, char* argv[] )
{
  int8_t t;

  // test rep
  printf("rep\n");
  t =    0; rep( t );
  t =   +1; rep( t );
  t =   -1; rep( t );
  t = +127; rep( t );
  t = -128; rep( t );
  printf("\n");

  // print all values of t, where t is an int8_t
  // int i must be created as an int of greater width than t must be used as else when t=-128 reached, t-- will cause t's value to loop, thus creating an infinite loop.
//   int i;
//   for (i = pow(2, BITSOF(t)) - 1; i > 0; i--)
//   {
//       rep( i );
//   }

  // t = 1; printf("%d\n", neg(t));
  // t = -1; printf("%d\n", neg(t));

  // test sign
  printf("sign\n");
  t =    0; printf("%d %d\n", t, sign(t));
  t =   +1; printf("%d %d\n", t, sign(t));
  t =   -1; printf("%d %d\n", t, sign(t));
  t = +127; printf("%d %d\n", t, sign(t));
  t = -128; printf("%d %d\n", t, sign(t));
  printf("\n");

  // test mod
  printf("mod\n");
  uint8_t x = 15; int n = 2;
  printf("%d %d %d\n", x, n, mod(t, n));
  printf("\n");

  // test int2seq
  printf("int2seq\n");
  bool X[8] = {};
  int number = int2seq(X, 0xFF);
  printf("X:\n");
  printBoolArray(X, 8);

  return 0;
}

void printBoolArray(bool *X, int n) {
    for (int i = 0; i < n; i++)
    {
        printf(X[i] ? "0" : "1");
        X++;
    }
    printf("\n");
}

int sign( uint8_t x )
{
    return ((x>>7)&1);
}

int8_t neg( int8_t x )
{
    if ((x>>7 & 1) == 1)
    {
        x &= 127; //127 in b=2 is 01111111, thus returns x flipped to 0, and all other bits the same.
    }
    return x;
}

uint8_t mod( uint8_t x, int n )
{
    return x ^ ((x >> n) << n);
    //return (uint8_t)( x << (BITSOF(x) - n) ) >> (BITSOF(x) - n);
}

// that should extract and then store each i-th bit of x in the i-th element of array X
// @return the total number of elements stored.
int int2seq( bool* X, int8_t x )
{
    int total = 0;
    for (int i = 0; i < BITSOF(x); i++)
    {
        printf("x>>%d: ", i); rep(x>>i); printf("\n");
        printf("x>>%d & 1", i); rep((x>>i)&1); printf("\n");
        X[i] = (x >> (i)) & 1;
        total++;
    }
    return total;
}
